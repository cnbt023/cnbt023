#include<stdio.h>
int main ()
{
  int sm[5][3], i, j = 0, max;
  for (i = 0; i < 5; i++)
    {
      printf ("Enter the marks of student no. %d in three subjects consecutively:\n", (i + 1));
      for (j = 0; j < 3; j++)
	{
	  scanf ("%d", &sm[i][j]);
	}

    }
  for (j = 0; j < 3; j++)
    {
      max = sm[0][j];
      for (i = 1; i < 5; i++)
	{
	  if (sm[i][j] > max)
	    {
	      max = sm[i][j];
	    }
	}
      printf ("The highest marks scored in subject %d is %d\n", (j + 1), max);
      max = 0;
    }
}


OUTPUT:
Enter the marks of student no. 1 in three subjects consecutively:                                                               
34                                                                                                                              
56                                                                                                                              
33                                                                                                                              
Enter the marks of student no. 2 in three subjects consecutively:                                                               
57                                                                                                                              
76                                                                                                                              
74                                                                                                                              
Enter the marks of student no. 3 in three subjects consecutively:                                                               
86                                                                                                                              
92                                                                                                                              
99                                                                                                                              
Enter the marks of student no. 4 in three subjects consecutively:                                                               
23                                                                                                                              
38                                                                                                                              
58                                                                                                                              
Enter the marks of student no. 5 in three subjects consecutively:                                                               
100                                                                                                                             
93                                                                                                                              
96                                                                                                                              
The highest marks scored in subject 1 is 100                                                                                    
The highest marks scored in subject 2 is 93                                                                                     
The highest marks scored in subject 3 is 99          