//write your code here#include <stdio.h>
#include <stdio.h>
int main ()
{
  int a[3][3], transpose[3][3], i, j;
  printf ("\nEnter matrix elements:\n");
  for (i = 0; i < 3; ++i)
    for (j = 0; j < 3; ++j)
      {
	printf ("Enter element a%d%d: ", i + 1, j + 1);
	scanf ("%d", &a[i][j]);
      }
  printf ("\nEntered matrix: \n");
  for (i = 0; i < 3; ++i)
    for (j = 0; j < 3; ++j)
      {
	printf ("%d  ", a[i][j]);
	if (j == 3 - 1)
	  printf ("\n");
      }
  for (i = 0; i < 3; ++i)
    for (j = 0; j < 3; ++j)
      {
	transpose[j][i] = a[i][j];
      }
  printf ("\nTranspose of the matrix:\n");
  for (i = 0; i < 3; ++i)
    for (j = 0; j < 3; ++j)
      {
	printf ("%d  ", transpose[i][j]);
	if (j == 3 - 1)
	  printf ("\n");
      }
  return 0;
  }
  
 OUTPUT:
 Enter matrix elements:                                                                                                          
Enter element a11: 1                                                                                                            
Enter element a12: 2                                                                                                            
Enter element a13: 3                                                                                                            
Enter element a21: 4                                                                                                            
Enter element a22: 5                                                                                                            
Enter element a23: 6                                                                                                            
Enter element a31: 7                                                                                                            
Enter element a32: 8                                                                                                            
Enter element a33: 9                                                                                                            
                                                                                                                                
Entered matrix:                                                                                                                 
1  2  3                                                                                                                         
4  5  6                                                                                                                         
7  8  9                                                                                                                         
                                                                                                                                
Transpose of the matrix:                                                                                                        
1  4  7                                                                                                                         
2  5  8                                                                                                                         
3  6  9                                                                                                                         
                         
