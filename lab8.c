struct student
{
    int age,roll_no;
    char department[50];
    char name[25];
}
stud[100];
void main()
{
    int i,n;
    printf("Enter the number of students:\n");
    scanf("%d",&n);
    printf("Enter student information (roll_no , name , age , department one by one, respectively)\n");
    for(i=0;i<n;i++)
    {
        scanf("%d %s %d %s",&stud[i].roll_no,stud[i].name,&stud[i].age,&stud[i].department);
    }
    printf("\nROLL_NO\t\tNAME\t\tAGE\t\tDEPARTMENT\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t\t%s\t\t%d\t\t%s\t\t\n",stud[i].roll_no,stud[i].name,stud[i].age,stud[i].department);
    }
}



OUTPUT:
Enter the number of students:                                                                                                   
3                                                                                                                               
Enter student information (roll_no , name , age , department one by one, respectively)                                          
1                                                                                                                               
RASHI                                                                                                                           
19                                                                                                                              
CSE                                                                                                                             
2                                                                                                                               
KARAN                                                                                                                           
19                                                                                                                              
ECE                                                                                                                             
3                                                                                                                               
ASHANA                                                                                                                          
18                                                                                                                              
BT                                                                                                                              
                                                                                                                                
ROLL_NO         NAME            AGE             DEPARTMENT                                                                      
1               RASHI           19              CSE                                                                             
2               KARAN           19              ECE                                                                             
3               ASHANA          18              BT                                                                              
                                                       