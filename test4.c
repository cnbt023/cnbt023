#include <stdio.h>
int main()
{
  int i, n, x, p = 1, temp;

  printf ("Enter a positive integer: ");
  scanf ("%d", &x);
  printf ("Enter the power: ");
  scanf ("%d", &n);
  for (i = 1; i <= n; i++)
    {
      temp = x;
      p = p * temp;
    }
  printf ("RESULT = %d", p);

  return 0;
} 


OUTPUT:
Enter a positive integer: 8                                                                                                   
Enter the power: 2                                                                                                            
RESULT = 64