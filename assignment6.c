
#include <stdio.h>
int main ()
{
  int i, fact = 1, m;
  printf ("Enter the number ");
  scanf ("%d", &m);
  for (i = 1; i <= m; i++)
    {
      fact = fact * i;
    }
  printf ("\nFactorial of the number is %d", fact);
  return 0;
}
 
 OUTPUT:
 Enter the number: 5 
Factorial of the number is 120 




#include <stdio.h>
int main()
{
  int n, a, r, s = 0;
  printf ("Enter a number of three digits:");
  scanf ("%d", &n);
  a = n;
  while (n > 0)
    {
      r = n % 10;
      s = s * 10 + r;
      n = n / 10;
    }
  printf ("\nReverse of the number %d is %d", a, s);
  return 0;
}

OUTPUT:
Enter a number of three digits:123
Reverse of the number 123 is 321



#include <stdio.h>
#include <math.h>

int main()
{
  int n, i;
  float sum = 0.0;
  printf ("Enter the value of n in the series: ");
  scanf ("%d", &n);
  for (i = 1; i <= n; i++)
    {
      sum = (sum+(1/pow(i,2)));
    }
  printf ("\nsum of the series is %f", sum);
  return 0;
}


OUTPUT:
Enter the value of n in the series: 5 
sum of the series is 1.463611



#include <stdio.h>

int main()
{
  int i, j;
  for (i = 1; i <= 5; i++)
    {
      for (j = 1; j <= i; j++)
	{
	  printf ("%d", i);
	}
      printf ("\n");
    }

  return 0;
}


OUTPUT:
1                                                                                                                             
22                                                                                                                            
333                                                                                                                           
4444                                                                                                                          
55555




#include<stdio.h>

void main()
{
  int i, j;

  for (i = 65; i <= 70; i++)
    {
      for (j = 65; j <= i; j++)
	{
	  printf ("%c", j);
	}

      printf ("\n");
    }
}


OUTPUT:
A                                                                                                                             
AB                                                                                                                            
ABC                                                                                                                           
ABCD                                                                                                                          
ABCDE                                                                                                                         
ABCDEF



#include <stdio.h>
int main()
{
  int n1, n2, divisor, dividend, remainderr;

  printf ("Enter two positive integers: ");
  scanf ("%d %d", &n1, &n2);
  if (n1 > n2)
    dividend = n1;
  else
    divisor = n2;

  while (divisor)
    {
      remainderr = dividend % divisor;
      dividend = divisor;
      divisor = remainderr;
    }
  printf ("GCD = %d", dividend);

  return 0;
}


OUTPUT:
Enter two positive integers: 67                                                                                               
99                                                                                                                            
GCD = 99