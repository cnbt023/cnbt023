#include <stdio.h>
int main()
{
  int i, n, x, sum = 0;

  printf ("Enter the value of n: ");
  scanf ("%d", &n);
  for (i = 1; i <= n; i++)
    {
      if(i%2==0)
      sum=(sum+(i*i));
    }
  printf ("Sum of squares of first %d even numbers is %d", n,sum);

  return 0;
}


OUTPUT:
Enter the value of n: 10                                                                                     
Sum of squares of first 10 even numbers is 220                 