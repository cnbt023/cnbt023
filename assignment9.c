#include<stdio.h>
#include<stdlib.h>
int main ()
{
  int i, j, k;
  int mat1[3][3], mat2[3][3], res[5][5];
  printf ("Enter the elements of First Matrix(3*3): \n");
  for (i = 0; i < 3; i++)
    {
      for (j = 0; j < 3; j++)
	scanf ("%d", &mat1[i][j]);
    }
  printf ("Enter the elements of Second Matrix(3*3): \n");
  for (i = 0; i < 3; i++)
    {
      for (j = 0; j < 3; j++)
	scanf ("%d", &mat2[i][j]);
    }
  for (i = 0; i < 3; i++)
    {
      j = 0;
      for (j = 0; j < 3; j++)
	{
	  res[i][j] = 0;
	  for (k = 0; k < 3; k++)
	    res[i][j] += mat1[i][k] * mat2[k][j];
	}
    }
  printf ("The Elements of the Product Matrix are: \n");
  for (i = 0; i < 3; i++)
    {
      printf ("\n");
      for (j = 0; j < 3; j++)
	printf ("\t%d", res[i][j]);
    }
  return 0;
}


OUTPUT:
Enter the elements of First Matrix(3*3):                                                                                        
1                                                                                                                               
2                                                                                                                               
3                                                                                                                               
4                                                                                                                               
5                                                                                                                               
6                                                                                                                               
7                                                                                                                               
8                                                                                                                               
9                                                                                                                               
Enter the elements of Second Matrix(3*3):                                                                                       
0                                                                                                                               
9                                                                                                                               
8                                                                                                                               
7                                                                                                                               
6                                                                                                                               
5                                                                                                                               
4                                                                                                                               
3                                                                                                                               
2                           
The Elements of the Product Matrix are:                                                                                         
                                                                                                                                
        26      30      24                                                                                                      
        59      84      69                                                                                                      
        92      138     114  