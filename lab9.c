//Program to enter a character and then determine whether it is a vowel or not using Pointers.
#include <stdio.h>
int main()
{
char str[100];
char *ptr;
printf("Enter a character: ");
scanf("%c",str);
ptr=str;
if(*ptr=='A' ||*ptr=='E' ||*ptr=='I' ||*ptr=='O' ||*ptr=='U' ||*ptr=='a' ||*ptr=='e' ||*ptr=='i' ||*ptr=='o' ||*ptr=='u')
printf("The character that you entered is a vowel.");
else
printf("The character that you entered is not a vowel.");
return 0;
}


OUTPUT:
Enter a character: s                                                                                                          
The character that you entered is not a vowel.     


//Program to swap two numbers using pointers.
#include<stdio.h>
void swap(int *, int *);
main()
{
    int x, y;
    printf("Enter the first number:");
    scanf("%d",&x);
    printf("Enter the second number:");
    scanf("%d",&y);
    printf("Before swapping, the two numbers are %d and %d\n",x,y);
    swap(&x,&y); 
    printf("After swapping, the two numbers are %d and %d\n",x,y);
}
swap(int *a, int *b)
{
    int t;
    t=*a;
    *a=*b;
    *b=t;
}


OUTPUT:
Enter the first number:200                                                                                                      
Enter the second number:500                                                                                                     
Before swapping, the two numbers are 200 and 500                                                                                
After swapping, the two numbers are 500 and 200                                                                                 
                                                       
