#include <stdio.h>
int main ()
{
  int num1, num2;
  int sum, sub, mult, mod;
  float div;
  int *x, *y;
  x = &num1;
  y = &num2;
  printf ("Enter any two numbers: ");
  scanf ("%d%d", x, y);
  sum = *x + *y;
  sub = *x - *y;
  mult = *x * *y;
  div = (float) *x / *y;
  mod = *x % *y;
  printf ("SUM = %d\n", sum);
  printf ("DIFFERENCE = %d\n", sub);
  printf ("PRODUCT = %d\n", mult);
  printf ("QUOTIENT = %f\n", div);
  printf ("MODULUS = %d", mod);
  return 0;
}



OUTPUT:
Enter any two numbers: 4                                                                                                        
2                                                                                                                               
SUM = 6                                                                                                                         
DIFFERENCE = 2                                                                                                                  
PRODUCT = 8                                                                                                                     
QUOTIENT = 2.000000                                                                                                             
MODULUS = 0    
