#include<stdio.h>
#define SIZE 100
int main()
{
  int arr[SIZE], i, n, ele, pos;
  printf ("Enter size of the array : \n");
  scanf ("%d", &n);
  printf ("Enter array elements: \n");
  for (i = 0; i < n; i++)
    {
      scanf ("%d", &arr[i]);
    }
  printf ("Enter the element to be inserted: \n");
  scanf ("%d", &ele);
  printf ("Enter position at which the element needs to be inserted: \n");
  scanf ("%d", &pos);
  for (i = n; i > pos; i--)
    {
      arr[i] = arr[i - 1];
    }
  arr[pos] = ele;
  n = n + 1;
  printf ("Array elements after insertion : \n");
  for (i = 0; i < n + 1; i++)
    {
      printf ("%d\n", arr[i]);
    }
return 0;
}


OUTPUT:
Enter size of the array :                                                                                                       
4                                                                                                                               
Enter array elements:                                                                                                           
1                                                                                                                               
2                                                                                                                               
3                                                                                                                               
4                                                                                                                               
Enter the element to be inserted:                                                                                               
0                                                                                                                               
Enter position at which the element needs to be inserted:                                                                       
0                                                                                                                               
Array elements after insertion :                                                                                                
0                                                                                                                               
1                                                                                                                               
2                                                                                                                               
3                                                                                                                               
4